![](http://i66.tinypic.com/2nhjju9.png)

## Software Oficina Técnica

### Documentos
 - Orden de Trabajo de Aplicación: Relacionar los documentos Orden de Trabajo con cada material solicitado.
 - Orden de Pedido: Relacionar los documentos Orden de Pedido con una Orden de Trabajo (opcional) y materiales

### Stock
 - Entrada: Registrar la entrada de material al stock actual, ya sea por parte de compra o por retiro del depósito.
 - Salida: Registrar la salida de material del stock actual por orden de trabajo.

### Modulos
 - Proveedor: Almacenar información relativa al proveedor por cada material y compra realizada.
 - Materiales: Realizar el alta, busqueda y proveer de stock mínimo para cada material y mantener actualizado el stock actual.
 - Personal: Almacenar y relacionar el personal que retira el material del depósito.

© 2017 - ET N°12 DE 1 Libertador General José de San Martín
